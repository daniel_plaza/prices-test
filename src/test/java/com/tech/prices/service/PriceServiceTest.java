package com.tech.prices.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import com.tech.prices.exception.UnknownPriceException;
import com.tech.prices.repository.PriceRepository;
import com.tech.prices.util.PriceParamsDataTestGenerator;
import com.tech.prices.web.dto.PriceDTO;
import com.tech.prices.web.dto.PriceParamsDTO;
import com.tech.prices.domain.Price;
import com.tech.prices.util.MapperTestUtil;
import com.tech.prices.util.PriceDataTestGenerator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@DisplayName("Price Service Unit Tests")
@ExtendWith(MockitoExtension.class)
public class PriceServiceTest {

    @Mock
    PriceRepository priceRepository;

    @Spy
    MapperFacade mapperFacade = MapperTestUtil.getMapperFacade();

    @InjectMocks
    PriceService underTest;

    @Test
    @DisplayName("Given brand, date and product should return price")
    void givenBrandDateAndProductShouldReturnPrice() {
        PriceParamsDTO input = PriceParamsDataTestGenerator.newPriceParams();
        Price price = PriceDataTestGenerator.newPrice();

        when(priceRepository.findPrice(input.getBrandId(), input.getDate(), input.getProductId()))
            .thenReturn(Collections.singletonList(price));

        PriceDTO result = underTest.checkPrice(input);

        verify(priceRepository, times(1)).findPrice(any(), any(), any());
        assertNotNull(result);
        assertEquals(price.getProductId(), result.getProductId());
        assertEquals(price.getBrandId(), result.getBrandId());
        assertEquals(price.getPriceList(), result.getPriceList());
        assertEquals(price.getStartDate(), result.getStartDate());
        assertEquals(price.getEndDate(), result.getEndDate());
        assertEquals(price.getPrice(), result.getPrice());
    }

    @Test
    @DisplayName("Given brand, date and product should with multiple results return highest priority price")
    void givenBrandDateAndProductWithMultipleShouldReturnHighestPriorityPrice() {
        PriceParamsDTO input = PriceParamsDataTestGenerator.newPriceParams();
        Price highestPriorityPrice = PriceDataTestGenerator.newPrice();
        highestPriorityPrice.setBrandId(99);
        Price lowPriorityPrice = PriceDataTestGenerator.newPrice();
        List<Price> prices = new ArrayList<>();
        prices.add(highestPriorityPrice);
        prices.add(lowPriorityPrice);

        when(priceRepository.findPrice(input.getBrandId(), input.getDate(), input.getProductId()))
            .thenReturn(prices);

        PriceDTO result = underTest.checkPrice(input);

        verify(priceRepository, times(1)).findPrice(any(), any(), any());
        assertNotNull(result);
        assertEquals(highestPriorityPrice.getProductId(), result.getProductId());
        assertEquals(highestPriorityPrice.getBrandId(), result.getBrandId());
        assertEquals(highestPriorityPrice.getPriceList(), result.getPriceList());
        assertEquals(highestPriorityPrice.getStartDate(), result.getStartDate());
        assertEquals(highestPriorityPrice.getEndDate(), result.getEndDate());
        assertEquals(highestPriorityPrice.getPrice(), result.getPrice());
    }

    @Test
    @DisplayName("Given brand, date and product not persisted should return error")
    void givenBrandDateAndProductNotPersistedShouldReturnError() {
        PriceParamsDTO input = PriceParamsDataTestGenerator.newPriceParams();

        when(priceRepository.findPrice(input.getBrandId(), input.getDate(), input.getProductId()))
            .thenReturn(Collections.emptyList());

        UnknownPriceException exception = assertThrows(UnknownPriceException.class,
            () -> underTest.checkPrice(input));

        verify(priceRepository, times(1)).findPrice(any(), any(), any());
        assertEquals(input.getBrandId(), exception.getBrandId());
        assertEquals(input.getDate(), exception.getDate());
        assertEquals(input.getProductId(), exception.getProductId());
    }

}
