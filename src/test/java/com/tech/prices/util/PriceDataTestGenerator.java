package com.tech.prices.util;

import com.tech.prices.domain.Price;
import com.tech.prices.domain.Currency;
import java.time.LocalDateTime;

public class PriceDataTestGenerator {

    private PriceDataTestGenerator() {
    }

    public static Price newPrice() {
        return Price.builder()
            .id(1l)
            .brandId(1l)
            .productId(5l)
            .priority(0)
            .priceList(1l)
            .price(10.0)
            .startDate(LocalDateTime.now())
            .endDate(LocalDateTime.now())
            .curr(Currency.EUR)
            .build();
    }

}
