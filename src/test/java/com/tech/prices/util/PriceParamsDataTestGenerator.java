package com.tech.prices.util;

import com.tech.prices.web.dto.PriceParamsDTO;
import java.time.LocalDateTime;
import java.util.stream.Stream;

public class PriceParamsDataTestGenerator {

    private PriceParamsDataTestGenerator(){}

    public static PriceParamsDTO newPriceParams(){
        return PriceParamsDTO.builder()
            .date(LocalDateTime.now())
            .productId(1l)
            .brandId(1l)
            .build();
    }

    public static Stream<PriceParamsDTO> wrongPriceParams(){
        return Stream.of(
            PriceParamsDTO.builder()
                .brandId(-1l)
                .productId(35455l)
                .date(LocalDateTime.parse("2020-06-14T16:00"))
                .build(),
            PriceParamsDTO.builder()
                .brandId(1l)
                .productId(-1l)
                .date(LocalDateTime.parse("2020-06-14T16:00"))
                .build(),
            PriceParamsDTO.builder()
                .brandId(1l)
                .productId(35455l)
                .date(LocalDateTime.parse("2019-06-14T16:00"))
                .build()
        );
    }

}
