package com.tech.prices.it;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tech.prices.web.dto.PriceDTO;
import com.tech.prices.web.dto.PriceParamsDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@DisplayName("Price Integration Tests")
@SpringBootTest
@AutoConfigureMockMvc
public class PriceIT {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @DisplayName("Day 14, 10:00 for product 35455 and brand 1 returns 35.50")
    void givenBrandDateAndProductShouldReturnSpecificPriceCaseOne() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}&date={date}&productId={product}",
                1, "2020-06-14T10:00", 35455))
            .andReturn().getResponse();
        PriceDTO result = objectMapper.readValue(response.getContentAsByteArray(), PriceDTO.class);

        assertEquals(200, response.getStatus());
        assertNotNull(result);
        assertEquals(35455,result.getProductId());
        assertEquals(1,result.getBrandId());
        assertEquals(1,result.getPriceList());
        assertNotNull(result.getStartDate());
        assertNotNull(result.getEndDate());
        assertEquals(35.50,result.getPrice());
    }

    @Test
    @DisplayName("Day 14, 16:00 for product 35455 and brand 1 returns 25.45")
    void givenBrandDateAndProductShouldReturnSpecificPriceCaseTwo() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}&date={date}&productId={product}",
                1, "2020-06-14T16:00", 35455))
            .andReturn().getResponse();
        PriceDTO result = objectMapper.readValue(response.getContentAsByteArray(), PriceDTO.class);

        assertEquals(200, response.getStatus());
        assertNotNull(result);
        assertEquals(35455,result.getProductId());
        assertEquals(1,result.getBrandId());
        assertEquals(2,result.getPriceList());
        assertNotNull(result.getStartDate());
        assertNotNull(result.getEndDate());
        assertEquals(25.45,result.getPrice());
    }

    @Test
    @DisplayName("Day 14, 21:00 for product 35455 and brand 1 returns 35.50")
    void givenBrandDateAndProductShouldReturnSpecificPriceCaseThree() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}&date={date}&productId={product}",
                1, "2020-06-14T21:00", 35455))
            .andReturn().getResponse();
        PriceDTO result = objectMapper.readValue(response.getContentAsByteArray(), PriceDTO.class);

        assertEquals(200, response.getStatus());
        assertNotNull(result);
        assertEquals(35455,result.getProductId());
        assertEquals(1,result.getBrandId());
        assertEquals(1,result.getPriceList());
        assertNotNull(result.getStartDate());
        assertNotNull(result.getEndDate());
        assertEquals(35.50,result.getPrice());
    }

    @Test
    @DisplayName("Day 15, 10:00 for product 35455 and brand 1 returns 30.50")
    void givenBrandDateAndProductShouldReturnSpecificPriceCaseFour() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}&date={date}&productId={product}",
                1, "2020-06-15T10:00", 35455))
            .andReturn().getResponse();
        PriceDTO result = objectMapper.readValue(response.getContentAsByteArray(), PriceDTO.class);

        assertEquals(200, response.getStatus());
        assertNotNull(result);
        assertEquals(35455,result.getProductId());
        assertEquals(1,result.getBrandId());
        assertEquals(3,result.getPriceList());
        assertNotNull(result.getStartDate());
        assertNotNull(result.getEndDate());
        assertEquals(30.50,result.getPrice());
    }

    @Test
    @DisplayName("Day 16, 21:00 for product 35455 and brand 1 returns 38.95")
    void givenBrandDateAndProductShouldReturnSpecificPriceCaseFive() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}&date={date}&productId={product}",
                1, "2020-06-16T21:00", 35455))
            .andReturn().getResponse();
        PriceDTO result = objectMapper.readValue(response.getContentAsByteArray(), PriceDTO.class);

        assertEquals(200, response.getStatus());
        assertNotNull(result);
        assertEquals(35455,result.getProductId());
        assertEquals(1,result.getBrandId());
        assertEquals(4,result.getPriceList());
        assertNotNull(result.getStartDate());
        assertNotNull(result.getEndDate());
        assertEquals(38.95,result.getPrice());
    }

    @ParameterizedTest
    @MethodSource("com.tech.prices.util.PriceParamsDataTestGenerator#wrongPriceParams")
    @DisplayName("Given params not persisted return not found")
    void givenWrongParamsReturnError(PriceParamsDTO priceParams) throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}&date={date}&productId={product}",
                priceParams.getBrandId(),
                priceParams.getDate(),
                priceParams.getProductId()))
            .andReturn().getResponse();

        assertEquals(404, response.getStatus());
    }

    @Test
    @DisplayName("Given wrong params return bad request")
    void givenWrongParamsReturnError() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders
            .get("/prices?brandId={brandId}", 1))
            .andReturn().getResponse();

        assertEquals(400, response.getStatus());
    }

}
