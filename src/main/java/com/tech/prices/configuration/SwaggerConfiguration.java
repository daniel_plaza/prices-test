package com.tech.prices.configuration;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	private static final String TITLE = "Prices test";
	private static final String DESCRIPTION = "Prices test API";
	private static final String VERSION = "1.0";
	private static final Contact CONTACT = new Contact(
		"Daniel Plaza",
		"https://gitlab.com/daniel_plaza",
		"daniel.plaza@kairosds.com"
	);

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.apiInfo(apiInfo())
			.select()
			.apis(RequestHandlerSelectors.basePackage("com.tech.prices"))
			.paths(PathSelectors.any())
			.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(TITLE, DESCRIPTION, VERSION, "", CONTACT, "", "",
			Collections.emptyList()
		);
	}

}
