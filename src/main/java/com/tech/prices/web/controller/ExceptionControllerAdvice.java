package com.tech.prices.web.controller;

import com.tech.prices.exception.UnknownPriceException;
import com.tech.prices.web.dto.ExceptionDTO;
import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionDTO defaultException(Exception e) {
        return ExceptionDTO.builder()
            .message(e.getMessage())
            .timestamp(LocalDateTime.now())
            .build();
    }

    @ExceptionHandler(UnknownPriceException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionDTO unknownPriceException(UnknownPriceException e) {
        return ExceptionDTO.builder()
            .message(String.format("Price not found for brand %s at date %s for product %s", e.getBrandId(), e.getDate(), e.getProductId()))
            .timestamp(LocalDateTime.now())
            .build();
    }

}
