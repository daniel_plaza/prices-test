package com.tech.prices.web.controller;

import com.tech.prices.web.PriceApi;
import com.tech.prices.web.dto.PriceDTO;
import com.tech.prices.web.dto.PriceParamsDTO;
import com.tech.prices.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prices")
@RequiredArgsConstructor
@Validated
public class PriceApiController implements PriceApi {

    private final PriceService priceService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PriceDTO checkPrice(PriceParamsDTO priceParams) {
        return priceService.checkPrice(priceParams);
    }

}
