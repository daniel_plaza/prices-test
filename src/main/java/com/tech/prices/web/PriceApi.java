package com.tech.prices.web;

import com.tech.prices.web.dto.PriceDTO;
import com.tech.prices.web.dto.PriceParamsDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;

@Api
public interface PriceApi {

    @ApiOperation(value = "Check price")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Price"),
        @ApiResponse(code = 400, message = "Invalid input"),
        @ApiResponse(code = 404, message = "Price not found"),
    })
    PriceDTO checkPrice(@Valid PriceParamsDTO numbers);

}
