package com.tech.prices.exception;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UnknownPriceException extends RuntimeException {

    private final Long brandId;

    private final LocalDateTime date;

    private final Long productId;


}
