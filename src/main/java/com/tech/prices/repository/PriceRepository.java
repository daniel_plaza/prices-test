package com.tech.prices.repository;

import com.tech.prices.domain.Price;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PriceRepository extends JpaRepository<Price, Long> {

    @Query("SELECT p FROM Price p WHERE p.brandId = :brandId AND p.productId = :productId AND :date BETWEEN p.startDate AND p.endDate ORDER BY p.priority DESC")
    List<Price> findPrice(@Param("brandId") Long brandId, @Param("date") LocalDateTime date,
        @Param("productId") Long productId);

}
