package com.tech.prices.service;

import com.tech.prices.domain.Price;
import com.tech.prices.exception.UnknownPriceException;
import com.tech.prices.repository.PriceRepository;
import com.tech.prices.web.dto.PriceDTO;
import com.tech.prices.web.dto.PriceParamsDTO;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PriceService {

    private final PriceRepository priceRepository;

    private final MapperFacade mapper;

    public PriceDTO checkPrice(PriceParamsDTO priceParams) {
        Optional<Price> queryResult = priceRepository
            .findPrice(priceParams.getBrandId(), priceParams.getDate(), priceParams.getProductId())
            .stream()
            .findFirst();

        return queryResult.map(price ->
            mapper.map(price, PriceDTO.class))
            .orElseThrow(
                () -> new UnknownPriceException(priceParams.getBrandId(), priceParams.getDate(),
                    priceParams.getProductId()));
    }

}
