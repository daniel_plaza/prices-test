FROM maven as builder
COPY . /code/
WORKDIR /code
RUN mvn package -DskipTests

FROM openjdk:8-jre-slim
COPY --from=builder /code/target/*.jar /usr/app/app.jar
WORKDIR /usr/app
EXPOSE 8080
CMD [ "java", "-jar", "app.jar" ]