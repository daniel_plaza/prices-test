# Getting Started

### Tests

* Run unit tests: ```mvn test```
* Run integration tests: ```mvn verify```

You can check jacoco reports at ```target/site``` then ```jacoco-ut``` for unit tests or ```jacoco-it``` for integration tests and open ```index.html``` in browser

### Deployment

Navigate via console your local path and execute:

* ```docker-compose build```

Once it is completed, the images has been build and you can run the app:

* ```docker-compose up -d```

And to stop the application:

* ```docker-compose down```

### Swagger API
Swagger API will be exposed only for documenting the API on 
``http://localhost:8080/swagger-ui.html`` 

### Using the API

Open a terminal and run this command:

```
curl -X GET http://localhost:8080/prices\?brandId\=1\&date\=2020-06-14T10:00\&productId\=35455
```
Query param values can me modified